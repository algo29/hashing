import 'package:crypto/crypto.dart';
import 'dart:convert';

void main() {
  var bytes = utf8.encode("Meow");
  var digest = sha1.convert(bytes);

  print("Digest as bytes: ${digest.bytes}");
  print("Digest as hex string: $digest");
}
